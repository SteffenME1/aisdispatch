import tkinter as tk
from tkinter import ttk
from tkinter.messagebox import showerror
from threading import Thread
import json
import requests
import sys
from pyais.stream import TCPConnection
import time

class assetAPI:
    
    base_url = "http://uomdvxf6dful5uja.myfritz.net:8000"

    # urls for token handling
    token_url = f"{base_url}/token/"
    token_refresh_url = f"{base_url}/token/refresh/"
    token_verify_url = f"{base_url}/token/verify/"  # check, if token is valid

    # Globals
    token = None
    refresh_token = None
    auth_header = None

    def __init__(self, username, password):
        
        self.session=requests.Session()
        self.session.headers.update({"Content-Type": "text/csv"})
        
        self.username=username
        self.password=password
        
        self.get_token_using_credentials()
        
    def get_token_using_credentials(self):
        """Request authorization token from the server before other requests can be done. 'token' is needed for subsequent requests to the server. 
        Client sends cretententials 'username' and 'password' to server. 
        Server responds with session 'token'. 
        Server also sends 'refresh_token'. refresh_token can be used to create new token if actual token had a timeout. (60 minutes typical)
            
        Return:
            token (str):
        """
        # get session token using cretentials username/password
        login_data = {
            "username": self.username,
            "password": self.password
        }

        response = requests.post(self.token_url, data=login_data)
        if response.status_code == 200:
            self.refresh_token = response.json().get("refresh")  # refresh_token valid for 1 day

            token = response.json().get("access")  # Access  token. Valid 1 hour
            self.session.headers.update({"Authorization": f"Bearer {token}"})

            return True
            
        
        else:
            print("Failed to obtain a token:", response.status_code,
                response.content.decode("utf-8"))
            return False
            
    def do_refresh_token(self):
        """Get a new token from the server. 
        'do_refresh_token' can be started, when actual access 'token' has expired (after 60 minutes)
        
        Parameter: refresh_token. (was created in 'get_token_using_credentials') 
        """
        response = requests.post(self.token_refresh_url, data={
                                'refresh': self.refresh_token})
        if response.status_code == 200:
            token = response.json().get('access')
            self.session.headers.update({"Authorization": f"Bearer {token}"})
            # print("Refreshed Access Token:", token)
        if response.status_code == 401:
            self.get_token_using_credentials()
        else:
            print("Failed to refresh the token:", response.status_code,
                response.content.decode("utf-8"))
    
    def read(self):
        data=[]
        adsb=self.get_data('adsb')
        ais=self.get_data('ais')
        
        for airplane in adsb:
            data.append(airplane)
        for ship in ais:
            data.append(ship)
        return data
        
    def get_data(self,source:str):
        url=f"{self.base_url}/{source}/get"
        response = self.session.get(url)
        if response.status_code == 200:
            print(response.content)
        elif response.status_code == 401:
            self.do_refresh_token()
            response = self.session.get(url)
            if response.status_code == 200:
                print(response.content)
            else: 
                return False
        else:
            print(response)
            
    def add_csv(self,csv:str, source:str):
        url = f"{self.base_url}/{source}/add/"
        response = self.session.post(url,data=csv)
        if response.status_code == 401:
            self.do_refresh_token()
            response = self.session.post(url,data=csv)
            if response.status_code != 200:
                return False

class AIS_Dispatcher():
    def __init__(self, host, port, data_max, interval:int):

        self.host = host
        self.port = port
        self.ships = {}
        try:
            with open('oldShips.json','r') as f:
                self.oldShips=json.load(f)
        except:
            self.oldShips={}
        self.active = True
        self.info=""
        self.data_used = 0
        self.data_max = data_max
        self.interval = interval

        self.api=assetAPI('read_write_user', 'adsb654321')
        
    def run(self):
        start=time.time()
        try:
            for msg in TCPConnection(self.host,port=self.port):
                if self.active==False:
                    return
                if self.data_used > self.data_max:
                    self.info="data used up"
                    return
                try:
                    ais_content=msg.decode()
                except:
                    continue
                data=self.get_data_as_dict(ais_content)
                if len(data) != 0:
                    self.update_ships(data)
                if (time.time()-start)>self.interval:
                    self.data_cleanup()
                    print(self.ships)
                    if len(self.ships.keys())>0:
                        self.send_csv()
                    start=time.time()
        except Exception as e:
            print(f'Error: {e}')
            self.info = e
            return
        return
    
    def start(self):
        with open('config.json','r') as f:
            config=json.load(f)
            self.data_used=config['data_used']
        self.active=True
        self.t=Thread(target=self.run)
        self.t.start()
    
    def stop(self):
        self.active=False
        with open('config.json','r') as f:
            config=json.load(f)
            config['data_used']=self.data_used
        with open('config.json','w') as f:
            json.dump(config,f)
        while self.t.is_alive():
            print('waiting for thread to die')
            time.sleep(1)
            pass
        return True
                
    def get_data_as_dict(self, msg):
        data={}
        if msg.msg_type<4:
            data={
                'timestamp':int(time.time()),
                'identifier':msg.mmsi,
                'lat':round(msg.lat,3),
                'lon':round(msg.lon,3),
                'cog':int(round(msg.course,0)),
                'sog':int(round(msg.speed,0))
            }
        return data
        if msg.msg_type==5:
            data={
                'timestamp':int(time.time()),
                'identifier':msg.mmsi,
                'name':msg.shipname,
                'shiptype':msg.ship_type
            }
        
    def update_ships(self, data):
        if not self.data_filter(data):
            return
        mmsi=data['identifier']
        if mmsi not in self.ships:
            self.ships[mmsi]=data
        else:
            for key, value in data.items():
                self.ships[mmsi][key]=value
            
    def send_csv(self):
        print('sendind csv')
        text="timestamp,identifier,lat,lon,cog,sog"
        update=self.get_changed_data()
        for ship in update:
            text=f"{text}\n{ship['timestamp']},{ship['identifier']},"
            if 'lat' in ship:
                text=f"{text}{ship['lat']}"
            text=f"{text},"
            if 'lon' in ship:
                text=f"{text}{ship['lon']}"
            text=f"{text},"
            if 'cog' in ship:
                text=f"{text}{ship['cog']}"
            text=f"{text},"
            if 'sog' in ship:
                text=f"{text}{ship['sog']}"
        print(text)
        self.info=text
        try:
            self.api.add_csv(text,'ais')
        except Exception as e:
            self.info=e
        
        self.data_used = self.data_used + len(text) + len(str(self.api.session.headers))
        return True
    
    def get_changed_data(self):
        update=[]
        for mmsi, ship in self.ships.items():
            if mmsi not in self.oldShips:
                update.append(self.ships[mmsi])
            else:
                update_ship={}
                updated=False
                oldShip=self.oldShips[mmsi]
                if ship['timestamp']==oldShip['timestamp']:
                    continue
                for key in ship:
                    if ship[key] != oldShip[key]:
                        if key=='cog':
                            if abs(ship[key]-oldShip[key])<3:
                                continue
                        
                        update_ship[key]=ship[key]
                        updated=True
                if updated:
                    update_ship['identifier']=mmsi
                    update.append(update_ship)
        self.oldShips=self.ships.copy()
        with open('oldShips.json', 'w') as f:
            json.dump(self.oldShips, f)
        return update
    
    def data_filter(self, ship):
        if 'lat' not in ship.keys():
            return True
        if ship['lat']>36.02 or ship['lat']<32.38:
            return False
        if ship['lon']>14.3 or ship['lon']<11.18:
            return False
        return True
    
    def data_cleanup(self):
        delete=[]
        for mmsi, ship in self.ships.items():
            if (time.time()-ship['timestamp'])>1800:
                delete.append(mmsi)
        for mmsi in delete:
            del self.ships[mmsi]
            
class App(tk.Tk):
    def __init__(self, config):
        super().__init__()
        
        self.host=config['host']
        self.port=config['port']
        self.interval=config['interval']
        self.config=config
        self.mbs_max=config['data_max']
        
        self.title('AIS Dispatch')
        self.geometry('680x550')
        self.resizable(1, 1)

        self.create_header_frame()
        self.create_body_frame()
        self.create_footer_frame()
        
        self.thread_alive=False

    def create_header_frame(self):

        self.header = ttk.Frame(self)
        # configure the grid
        self.header.columnconfigure(0, weight=1)
        self.header.columnconfigure(1, weight=10)
        self.header.columnconfigure(2, weight=1)
        
        # host
        self.host_label = ttk.Label(self.header, text='Host IP')
        self.host_label.grid(column=0, row=0, sticky=tk.W)
        
        self.host_var = tk.StringVar()
        self.host_var.set(self.host)
        self.host_entry = ttk.Entry(self.header,
                                   textvariable=self.host_var,
                                   width=40)
        self.host_entry.grid(column=1, row=0, sticky=tk.EW)
        
        # port
        self.port_label = ttk.Label(self.header, text='Port')
        self.port_label.grid(column=0, row=1, sticky=tk.W)
        
        self.port_var = tk.StringVar()
        self.port_var.set(self.port)
        self.port_entry = ttk.Entry(self.header, textvariable=self.port_var,
                                   width=40)

        self.port_entry.grid(column=1, row=1, sticky=tk.EW)
        
        # mbs used
        self.mb_label = ttk.Label(self.header, text=f'Maximum Data')
        self.mb_label.grid(column=0, row=2, sticky=tk.W)
        
        self.mbs_var = tk.StringVar()
        self.mbs_var.set(str(self.mbs_max))
        self.mbs_entry = ttk.Entry(self.header, textvariable=self.mbs_var,
                                   width=40)

        self.mbs_entry.grid(column=1, row=2, sticky=tk.EW)
        
        # mbs used
        self.mb_used_label = ttk.Label(self.header, text=f'Data used')
        self.mb_used_label.grid(column=0, row=3, sticky=tk.W)
        
        self.mbs_used=tk.Text(self.header,height=1, width=40)
        self.mbs_used.grid(column=1,row=3, sticky=tk.EW )
        
        # interval
        self.interval_label = ttk.Label(self.header, text=f'Interval in s')
        self.interval_label.grid(column=0, row=4, sticky=tk.W)
        
        self.interval_var = tk.StringVar()
        self.interval_var.set(str(self.interval))
        self.interval_entry = ttk.Entry(self.header, textvariable=self.interval_var,
                                   width=40)

        self.interval_entry.grid(column=1, row=4, sticky=tk.EW)
        
        
        # attach the header frame
        self.header.grid(column=0, row=0, sticky=tk.NSEW, padx=10, pady=10)

    def handle_dispatch(self, ais):
        if not self.send:
            if ais.stop():
                if not ais.t.is_alive():
                    self.thread_alive=False
            return
        if ais.t.is_alive():
            self.thread_alive=True
        else:
            self.thread_alive=False
            self.stop_send()
        self.html.delete(1.0, tk.END)
        self.html.insert(tk.END,ais.info)
        self.mbs_used.delete(1.0,tk.END)
        self.mbs_used.insert(tk.END,str(ais.data_used))
        if ais.data_used>self.mbs_max:
            self.html.delete(1.0, tk.END)
            self.html.insert(tk.END,'all data used')
            if ais.stop():
                if not ais.t.is_alive():
                    self.thread_alive=False
            self.stop_send()
        
        self.after(1000, lambda: self.handle_dispatch(ais))
            
    def stop_send(self):
        self.send=False
        self.start_button['state']=tk.ACTIVE
        self.stop_button['state']=tk.DISABLED
        if self.thread_alive:
            print('GUI waiting for thread to close')
            self.after(1000,self.stop_send)
            return
        print('all closed')
        self.exit_button['state']=tk.ACTIVE

        
    def start_send(self):
        self.send=True
        self.start_button['state']=tk.DISABLED
        self.exit_button['state']=tk.DISABLED
        self.stop_button['state']=tk.ACTIVE
        
        self.host = self.host_var.get()
        self.port = self.port_var.get()
        self.mbs_max = int(self.mbs_var.get())
        self.interval = int(self.interval_var.get())
        
        try:
            self.port = int(self.port)
        except:
            showerror(title='Error',
                      message='Please enter valid the Port')
        
        ais = AIS_Dispatcher(self.host,self.port,self.mbs_max, self.interval)
        ais.start()
        self.handle_dispatch(ais)
        return self.send

    def create_body_frame(self):
        self.body = ttk.Frame(self)
        # text and scrollbar
        self.html = tk.Text(self.body, height=20)
        self.html.grid(column=0, row=1)

        scrollbar = ttk.Scrollbar(self.body,
                                  orient='vertical',
                                  command=self.html.yview)

        scrollbar.grid(column=1, row=1, sticky=tk.NS)
        self.html['yscrollcommand'] = scrollbar.set

        # attach the body frame
        self.body.grid(column=0, row=1, sticky=tk.NSEW, padx=10, pady=10)

    def create_footer_frame(self):
        self.footer = ttk.Frame(self)
        # configure the grid
        self.footer.columnconfigure(0, weight=1)
        # exit button
        
        self.start_button = ttk.Button(self.footer,
                                      text='Start',
                                      command=self.start_send)
        self.start_button.grid(column=0, row=0, sticky=tk.E)

        self.stop_button = ttk.Button(self.footer,
                                      text='Stop',
                                      state=tk.DISABLED,
                                      command=self.stop_send)
        self.stop_button.grid(column=1, row=0, sticky=tk.E)

        self.exit_button = ttk.Button(self.footer,
                                      text='Exit',
                                      command=self.clean_exit)
        self.exit_button.grid(column=2, row=0, sticky=tk.E)

        # attach the footer frame
        self.footer.grid(column=0, row=2, sticky=tk.NSEW, padx=10, pady=10)
        
    def clean_exit(self):
        self.stop_send()
        self.destroy()
        

if __name__ == "__main__":
    with open('config.json', 'r') as f:
        config=json.load(f)
    app = App(config)

    app.mainloop()